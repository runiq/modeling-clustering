#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Performs a clustering run with a number of clusters and a given mask,
and creates graphs of the corresponding DBI, pSF, SSR/SST, and RMSD
values.

These faciliate the choice of cluster numbers and improve the clustering
process by allowing to pick the number of clusters with the highest
information content.
"""

# TODO
# - Fix plot_tree()
# - Do some logging
# - remove clustering_run from plot_metrics() and plot_tree() as it
#   basically represents world state. Use explicit metrics/nodes instead
# - Implement ylabel alignment as soon as PGF backend has its act together

import cStringIO as csio
from glob import glob, iglob
import os
import os.path as op
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as tic
import matplotlib.transforms as tfs


import clustering_run as cr
import newick as cn


def align_yaxis_labels(axes, sortfunc):
    xpos = sortfunc(ax.yaxis.get_label().get_position()[0] for ax in axes)
    for ax in axes:
        trans = tfs.blended_transform_factory(tfs.IdentityTransform(), ax.transAxes)
        ax.yaxis.set_label_coords(xpos, 0.5, transform=trans)


def plot_metrics(clustering_run, output_file, xmin=None, xmax=None,
        use_tex=False, figsize=(12,8), square=False):
    metrics = clustering_run.gather_metrics()
    # The ±0.5 are so that all chosen points are well within the
    # plots
    if xmin is None:
        xmin = min(metrics['n'])
    if xmax is None:
        xmax = max(metrics['n'])
    xlim = (xmin-0.5, xmax+0.5)

    fig = plt.figure(figsize=figsize)

    if clustering_run.no_ssr_sst:
        gridindex = 310
    else:
        if square:
            gridindex = 220
        else:
            gridindex = 410

    if use_tex:
        rmsd_ylabel = r'Critical distance/\si{\angstrom}'
        xlabel = r'$n_{\text{Clusters}}$'
    else:
        rmsd_ylabel = u'Critical distance/Å'
        xlabel = r'Number of clusters'

    ax1 = fig.add_subplot(gridindex+1, ylabel=rmsd_ylabel)
    ax2 = fig.add_subplot(gridindex+2, ylabel='DBI', sharex=ax1)
    ax3 = fig.add_subplot(gridindex+3, ylabel='pSF', sharex=ax1)
    ax1.plot(metrics['n'], metrics['rmsd'], marker='.')
    ax2.plot(metrics['n'], metrics['dbi'], marker='.')
    ax3.plot(metrics['n'], metrics['psf'], marker='.')
    if not clustering_run.no_ssr_sst:
        ax4 = fig.add_subplot(gridindex+4,
                ylabel='SSR/SST', xlim=xlim, sharex=ax1)
        ax4.plot(metrics['n'], metrics['ssr_sst'], marker='.')

    if square and not clustering_run.no_ssr_sst:
        nonxaxes = fig.axes[:-2]
        xaxes = fig.axes[-2:]
        lefthandplots = fig.axes[0::2]
        righthandplots = fig.axes[1::2]
        # Put yticklabels of right-hand plots to the right
        for ax in righthandplots:
            ax.yaxis.tick_right()
            ax.yaxis.set_label_position('right')
    else:
        nonxaxes = fig.axes[:-1]
        xaxes = [fig.axes[-1]]
        lefthandplots = fig.axes

    # xaxes limits and tick locations are propagated across sharex plots
    for ax in xaxes:
        ax.set_xlabel(xlabel)
        ax.xaxis.set_major_locator(tic.MultipleLocator(10))
        ax.xaxis.set_minor_locator(tic.AutoMinorLocator(2))
    for ax in nonxaxes:
        plt.setp(ax.get_xticklabels(), visible=False)
    # 5 yticklabels are enough for everybody
    for ax in fig.axes:
        ax.yaxis.set_major_locator(tic.MaxNLocator(nbins=5))
        ax.yaxis.set_minor_locator(tic.MaxNLocator(nbins=5))

    # Draw first to get proper ylabel coordinates
    # fig.canvas.draw()
    # align_yaxis_labels(lefthandplots, sortfunc=min)
    # if square and not clustering_run.no_ssr_sst:
    #     align_yaxis_labels(righthandplots, sortfunc=max)
    fig.savefig(output_file)


def plot_tree(clustering_run, node_info, steps, dist, output, graphical=None, no_length=False):
    tree = cn.parse_clustermerging(clustering_run)
    newick = tree.create_newick(node_info=node_info, no_length=no_length, steps=steps, dist=dist)
    if output is sys.stdout:
        fh = output
    else:
        fh = open(output, 'w')
    fh.write(newick)
    fh.close()
    fig = plt.figure()

    ax1 = fig.add_subplot(111, ylabel='Cluster tree')
    if graphical is not None:
        cn.draw(csio.StringIO(newick), do_show=False, axes=ax1)
    fig.savefig(graphical)


def parse_args():
    import argparse as ap
    parser = ap.ArgumentParser()
    parser.add_argument('-c', '--cm-file', metavar='FILE',
            default='./ClusterMerging.txt', dest='cm_fn',
            help="File to parse (default: ./ClusterMerging.txt)")
    parser.add_argument('-C', '--matplotlibrc', metavar='FILE', default=None,
            help="Matplotlibrc file to use")
    parser.add_argument('-p', '--prefix', default='c',
            help="Prefix for clustering result files (default: \"c\")")
    parser.add_argument('-N', '--no-ssr-sst', action='store_true', default=False,
            help="Don't gather SSR_SST values (default: False)")

    subs = parser.add_subparsers(dest='subcommand', help="Sub-command help")

    c = subs.add_parser('cluster', help="Do clustering run to gather metrics")
    c.add_argument('prmtop', help="prmtop file")
    c.add_argument('-m', '--mask', metavar='MASKSTR', default='@CA,C,O,N',
            help=("Mask string (default: '@CA,C,O,N')"))
    c.add_argument('-P', '--ptraj-trajin-file', metavar='FILE',
            default='ptraj_trajin', dest='ptraj_trajin_fn',
            help=("Filename for ptraj trajin file (default: ptraj_trajin)"))
    c.add_argument('-n', '--num-clusters', dest='n_clusters', type=int,
            metavar='CLUSTERS', default=50,
            help="Number of clusters to examine (default (also maximum): 50)")
    c.add_argument('-s', '--start-num-clusters', dest='start_n_clusters',
            type=int, metavar='CLUSTERS', default=2,
            help="Number of clusters to start from (default: 2)")
    c.add_argument('-l', '--logfile', metavar='FILE', default=None,
            dest='log_fn',
            help=("Logfile for ptraj run (default: Print to stdout)"))
    c.add_argument('--use-cpptraj', action='store_true', default=False,
            help="Use cpptraj instead of ptraj")

    t = subs.add_parser('tree', help="Create Newick tree representation")
    t.add_argument('-o', '--output', metavar='FILE', default=sys.stdout,
            help="Output file for Newick tree (default: print to terminal)")
    t.add_argument('-g', '--graphical', default=None,
            help="Save tree as png (default: Don't)")
    t.add_argument('-s', '--steps', type=int, default=None,
            help="Number of steps to print (default: all)")
    t.add_argument('-d', '--dist', type=float, default=None,
            help="Minimum distance to print (default: all)")
    t.add_argument('-i', '--node-info', choices=['num', 'dist', 'id'],
            default='num', help="Node data to print")
    t.add_argument('-l', '--no-length', default=False, action='store_true',
            help="Don't print branch length information")

    p = subs.add_parser('plot', help="Plot clustering metrics")
    p.add_argument('-o', '--output', metavar='FILE',
            default='clustering_metrics.png',
            help="Filename for output file (default: show using matplotlib)")
    p.add_argument('-n', '--num-clusters', dest='n_clusters', type=int,
            metavar='CLUSTERS', default=50,
            help="Number of clusters to examine (default (also maximum): 50)")
    p.add_argument('-s', '--start-num-clusters', dest='start_n_clusters',
            type=int, metavar='CLUSTERS', default=2,
            help="Number of clusters to start from (default: 2)")
    p.add_argument('-T', '--use-tex', default=False, action='store_true',
            help="Use LaTeX output (default: use plaintext output)")
    p.add_argument('-S', '--fig-size', nargs=2, type=float, metavar='X Y', default=[12, 8],
            help=("Figure size in inches (default: 12x8)"))
    p.add_argument('--square', default=False, action='store_true',
            help="Plot in two columns")
    return parser.parse_args()


def main():
    args = parse_args()
    if args.matplotlibrc is not None:
        matplotlib.rc_file(args.matplotlibrc)

    if args.subcommand == 'cluster':
        if args.n_clusters < 1 or args.n_clusters > 50:
            print "Error: Maximum cluster number must be between 1 and 50."
            sys.exit(1)

        cn_fns = None
        clustering_run = cr.ClusteringRun(prmtop=args.prmtop,
                start_n_clusters=args.start_n_clusters, n_clusters=args.n_clusters,
                cm_fn=args.cm_fn, mask=args.mask,
                ptraj_trajin_fn=args.ptraj_trajin_fn, cn_fns=cn_fns,
                prefix=args.prefix, log_fn=args.log_fn,
                no_ssr_sst=args.no_ssr_sst)
    else:
        if not op.exists(args.cm_fn):
            print ("{cm_fn} doesn't exist. Please perform a clustering run",
                    "first.".format(cm_fn=args.cm_fn))
            sys.exit(1)

        # We assume that the number of clusters starts at 1
        n_clusters = len(glob('{prefix}*.txt'.format(prefix=args.prefix)))
        cn_fns = {i: '{prefix}{n}.txt'.format(prefix=args.prefix, n=i) for
                i in xrange(1, n_clusters+1)}

        # Only cm_fn and cn_fns are necessary for plotting the tree and
        # metrics
        clustering_run = cr.ClusteringRun(prmtop=None, cm_fn=args.cm_fn,
                cn_fns=cn_fns, no_ssr_sst=args.no_ssr_sst)

    if args.subcommand == 'plot':
        plot_metrics(clustering_run, output_file=args.output,
                xmin=args.start_n_clusters, xmax=args.n_clusters,
                use_tex=args.use_tex, figsize=args.fig_size,
                square=args.square)
    elif args.subcommand == 'tree':
        plot_tree(clustering_run=clustering_run, node_info=args.node_info,
                steps=args.steps, dist=args.dist, no_length=args.no_length,
                graphical=args.graphical, output=args.output)


if __name__ == '__main__':
    main()
