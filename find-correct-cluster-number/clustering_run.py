# TODO:
# - Figure out when to use previous runs' information
# - merge this module's parse_clustermerging and
# newick.parse_clustermerging

from StringIO import StringIO
from os import remove
import shutil
import os.path as op
import subprocess as sp
import sys

import numpy as np


CM_FILE = 'ClusterMerging.txt'


class ClusteringRun(object):
    def __init__(self, prmtop=None, mask='@CA,C,O,N', start_n_clusters=2,
            n_clusters=50, ptraj_trajin_fn='ptraj_trajin', cm_fn=CM_FILE,
            cn_fns=None, prefix='c', log_fn=None, no_ssr_sst=False,
            use_cpptraj=False):
        self.prmtop = prmtop
        self.mask = mask
        self.start_n_clusters = start_n_clusters
        self.n_clusters = n_clusters
        self.ptraj_trajin_fn = ptraj_trajin_fn
        self.cm_fn = cm_fn
        self.no_ssr_sst = no_ssr_sst
        if use_cpptraj:
            self._ptraj_prg = 'cpptraj'
        else:
            self._ptraj_prg = 'ptraj'
        if cn_fns is None:
            self.cn_fns = {}
        else:
            self.cn_fns = cn_fns
        self.prefix = prefix
        self.log_fn = log_fn

        # No clustering run necessary if all of the following conditions
        # apply:
        # - ClusterMerging.txt exists
        # - self.cn_fns is not empty
        # - every file in self.cn_fns exists
        # Incidentally, fuck yeah generators. They make code look like
        # Lisp, however. http://xkcd.com/297/
        if all((op.exists(self.cm_fn),
               self.cn_fns,
               all(op.exists(fn) for fn in self.cn_fns.itervalues()))):
            pass
        else:
            self.cluster()
            pass
        self.n_decoys = self._get_n_decoys()

    def _get_n_decoys(self):
        with open(self.cm_fn) as cm_file:
            for i, _ in enumerate(cm_file, start=1):
                # i is the number of nodes in the clustering tree and is
                # always 1 lower than the number of decoys if we cluster
                # up to a single cluster
                # Did that just make sense to anyone?
                pass
            self.n_decoys = i + 1
            return self.n_decoys

    def _cluster(self, script, append=False):
        def _run_ptraj(script_fn):
            # God I hate plumbing
            writemode = 'a' if append else 'w'
            if self.log_fn is None:
                log_fh = self.log_fn
            else:
                log_fh = open(self.log_fn, writemode)
                with log_fh as logfile:
                    return sp.check_call([self._ptraj_prg, self.prmtop,
                            script_fn], stdout=logfile, stderr=logfile)

        try:
            remove('ptraj_script')
        except OSError as e:
            if e.errno == 2:
                pass

        shutil.copy(self.ptraj_trajin_fn, 'ptraj_script')
        with open('ptraj_script', 'a') as ptraj_script:
            ptraj_script.write(script)
        return _run_ptraj(script_fn='ptraj_script')

    def _run_c1(self):
        ptraj_single = ('cluster out c1 representative none average none all '
                'none averagelinkage clusters 1 rms {mask}'.format(mask=self.mask))
        self._cluster(script=ptraj_single)
        self.cn_fns[1] = '{prefix}1.txt'.format(prefix=self.prefix)
        return self.cn_fns

    def _run_cn(self):
        clusterstring = ('cluster out c{n} representative none average none all '
                'none ReadMerge clusters {n} rms {mask}')
        ptraj_full = ('\n'.join(clusterstring.format(n=i, mask=self.mask) for i
            in xrange(self.start_n_clusters, self.n_clusters+1)))
        self._cluster(script=ptraj_full)
        self.cn_fns.update({i: 'c{n}.txt'.format(n=i) for i in
                xrange(self.start_n_clusters, self.n_clusters+1)})
        return self.cn_fns

    def _parse_clustermerging(self, reverse=True):
        clustermerging = np.genfromtxt(self.cm_fn,
                # - pSF values are only computed for the last 50 records
                # - The results for n_cluster = 1 are not helpful
                skip_header=self.n_decoys - 51, skip_footer=1,
                dtype=[('n', 'i8'), ('rmsd', 'f8'), ('dbi', 'f8'), ('psf', 'f8')],
                usecols=(0, 3, 4, 5), invalid_raise=False,
                converters={0: lambda s: int(s.rstrip(':')) + self.n_decoys + 1})
        if reverse:
            step = -1
        else:
            step = 1
        self._n = clustermerging['n'][::step]
        self._dbi = clustermerging['dbi'][::step]
        self._psf = clustermerging['psf'][::step]
        self._rmsd = clustermerging['rmsd'][::step]

    def _get_ssr_ssts(self):
        ssr_ssts = []
        for i, fn in sorted(self.cn_fns.iteritems()):
            with open(fn) as fh:
                for line in fh:
                    if line.startswith('#SSR/SST: '):
                        ssr_sst_pre = line.split()[1]
                        # '-nan' is the value for a single cluster
                        if ssr_sst_pre != '-nan':
                            ssr_sst = float(ssr_sst_pre)
                            ssr_ssts.append(ssr_sst)
        self._ssr_sst = np.array(ssr_ssts)

    def cluster(self):
        if self.n_clusters == 1:
            self._run_c1()
        elif (self.n_clusters > 1 and op.exists(self.cm_fn) and
                not self.no_ssr_sst):
            # If we don't want to plot SSR/SST values, the subsequent
            # clustering runs aren't necessary, because all other
            # metrics are present in ClusterMerging.txt already
            self._run_cn()
        else:
            print "No valid cluster number specified"
            sys.exit(1)

    def gather_metrics(self):
        self._parse_clustermerging()
        if self.no_ssr_sst:
            imetrics = zip(self._n, self._rmsd, self._dbi, self._psf)
            self.metrics = np.rec.fromrecords(imetrics,
                    names=('n', 'rmsd', 'dbi', 'psf'))
        else:
            self._get_ssr_ssts()
            imetrics = zip(self._n, self._rmsd, self._dbi, self._psf,
                    self._ssr_sst)
            self.metrics = np.rec.fromrecords(imetrics,
                    names=('n', 'rmsd', 'dbi', 'psf', 'ssr_sst'))
        return self.metrics
