#!/bin/sh

TARGET=target

echo "Create cpptraj trajin input file"
for i in ../modeling/${TARGET}.B9999????.pdb; do
    echo "trajin \"$i\"" >> ptraj_trajin
done

echo "Do actual clustering run"
echo "Single cluster with averagelinkage algorithm"
./plot_clustering_metrics.py --prefix c cluster ../modeling/${TARGET}.B99990001.pdb \
    --ptraj-trajin-file ptraj_trajin --mask ':1-999' \
    --num-clusters 1 --logfile clustering.log \
	--use-cpptraj
echo "Multiple clusters with ReadMerge algorithm"
./plot_clustering_metrics.py --prefix c cluster ../modeling/${TARGET}.B99990001.pdb \
    --ptraj-trajin-file ptraj_trajin --mask ':1-999' \
    --num-clusters 50 --logfile clustering.log \
	--use-cpptraj

echo "Delete concatenated ptraj script file"
rm ptraj_trajin
rm ptraj_script

echo "Plot clustering metrics"
./plot_clustering_metrics.py --prefix c plot --output clustering_metrics.png

echo "Done. The plotted metrics are in clustering_metrics.png."
