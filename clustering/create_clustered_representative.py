#!/usr/bin/env python

"""
Uses MODELLER to create an averaged, conjugate gradients-optimized
representative structure of a cluster.
"""

import os
import os.path as op
from glob import glob

from modeller import *
from modeller.automodel import autosched
from modeller.scripts import complete_pdb


def parse_summary_file(summary_file):
    """
    Returns a dictionary with cluster ids as keys, and lists of the
    members of each cluster as values. For example,

        clusters[0] = [0, 2, 4, 5]

    means that the cluster with id 0 has decoys with ids 0, 2, 4,
    and 5 in it.
    """
    with open(summary_file) as summary_fh:
        clusters = {}
        for line in summary_fh:
            # First line that starts with '#Clustering: ' begins the
            # distribution table and contains number of clusters
            if line.startswith('#Clustering: '):
                n_clusters = int(line.split()[1])
                # We want to process exactly as many lines as there are
                # clusters
                for c in xrange(n_clusters):
                    line = summary_fh.next()
                    # Fuck yeah generators
                    clusters[c] = (i for (i, char) in enumerate(line)
                            if char == 'X')
                # We don't need any more information after that
                break
    return clusters


def create_decoy_filenames(clusters, file_stem, decoy_dir):
    decoy_list = glob(op.join(decoy_dir, '{stem}????.pdb'.format(stem=file_stem)))
    # We need the list to be sorted
    decoy_list = sorted(decoy_list)
    for c, decoys in clusters.iteritems():
        # Incidentially, fuck yeah generators
        yield (c, [decoy_list[i] for i in decoys])


def cluster_pdbs(e, pdbs, repname):
    """Get a representative of a set of PDBs.
       Every PDB file must be a structure of the same sequence.
       The representative model is returned."""
    a = alignment(e)

    # Read all structures, and make a 1:1 alignment of their sequences
    for filename in pdbs:
        m = complete_pdb(e, filename)
        a.append_model(m, align_codes=filename, atom_files=filename)

    # Structurally superimpose all structures without changing the alignment
    a.malign3d(gap_penalties_3d=(0, 3), fit=False)

    # Add a new dummy model with the same sequence to hold the cluster
    m = complete_pdb(e, pdbs[0])
    a.append_model(m, align_codes='cluster', atom_files=repname)

    # Make the clustered representative
    m.transfer_xyz(a, cluster_cut=-1)
    return m


def create_links(cluster, pdbs, cluster_dir, link_func='copy', rel_link=False):
    cur_cluster_dir = op.join(cluster_dir, 'c' + str(cluster))
    os.makedirs(cur_cluster_dir)
    for pdb in pdbs:
        pdb_basename = op.basename(pdb)
        pdb_link_fn = op.join(cur_cluster_dir, pdb_basename)
        if link_func == 'copy':
            shutil.copy2(pdb, pdb_link_fn)
        elif link_func == 'symlink':
            if rel_link:
                pdb_link_target = op.relpath(pdb, pdb_link_fn)
            else:
                pdb_link_target = pdb
            os.symlink(pdb_link_target, pdb_link_fn)
        elif link_func = 'hardlink':
            pdb_link_target = pdb
            os.link(pdb_link_target, pdb_link_fn)


def optimize_with_vtfm(e, mdl):
    # Select all atoms
    atmsel = selection(mdl)

    # Generate the stereochemical restraints
    # We only want stereochemical restraints, not the homology derived
    # ones
    mdl.restraints.make(atmsel, restraint_type='stereo', spline_on_site=False)
    # Create optimizer objects and set defaults for all further optimizations
    schedule = autosched.slow
    mysched = schedule.make_for_model(mdl) * e.schedule_scale
    for step in mysched:
        step.optimize(atmsel, output='REPORT', max_iterations=200)
    return mdl


def parse_args():
    import argparse as ap
    p = ap.ArgumentParser()
    p.add_argument('summary_file', help="Summary file to parse")
    p.add_argument('-f', '--file-stem', default='B9999',
            help="File stem for decoys (default: B9999)")
    p.add_argument('-d', '--decoy-dir', default='.',
            help="Directory where decoy PDB files reside (default: '.')")
    p.add_argument('-D', '--cluster-dir', default=None,
            help=("Directory to symlink cluster members to "
                    "(default: Don't do that)"))
    p.add_argument('-F', '--link-func', default='copy',
            choices=['copy', 'symlink', 'hardlink'],
            help=("Copy cluster members, hardlink them, or symlink them"
                " (default: copy; only works with --cluster-dir)"))
    p.add_argument('-R', '--relative', default=False, action='store_true',
            help=("Use relative symlinks "
                    "(only works with --link-func symlink)"))
    return p.parse_args()


def main():
    args = parse_args()
    clusters = parse_summary_file(args.summary_file)
    e = environ()
    e.libs.topology.read(file='$(LIB)/top_heav.lib')
    e.libs.parameters.read(file='$(LIB)/par.lib')

    for cluster, pdbs in create_decoy_filenames(clusters, args.file_stem,
            args.decoy_dir):
        if args.cluster_dir is not None:
            create_links(cluster, pdbs, args.cluster_dir,
                    link_func=args.link_func, rel_link=args.relative)
        repname = 'c' + str(cluster) + '.rep.pdb'
        m = cluster_pdbs(e, pdbs, repname)
        m = optimize_with_vtfm(e, m)
        m.write(file=repname)


if __name__ == '__main__':
    main()
