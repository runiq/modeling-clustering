#!/bin/bash

TARGET=target
TEMPLATE=template

echo "Running ptraj clustering"

echo "Creating ptraj_trajin file"
for i in ../modeling/${TARGET}.B9999????.pdb; do
    echo "trajin $i" >> ptraj_trajin
done

if [ -r ../find-correct-cluster-number/PairwiseDistances ]; then
    echo "Symlinking distance matrix and clustering run from previous run"
    ln -s ../find-correct-cluster-number/PairwiseDistances .
    ln -s ../find-correct-cluster-number/ClusterMerging.txt .
else
    echo "Using Averagelinkage algorithm instead of ReadMerge"
    sed -i 's/ReadMerge/averagelinkage/' ptraj_cluster
fi

echo "Creating ptraj_full"
cat ptraj_trajin ptraj_cluster > ptraj_full

echo "Clustering into 4 clusters
- using the ReadMerge algorithm
- backbone RMSD fit (@CA,C,O,N)
- writing out representative structures as PDB files"

ptraj ../modeling/${TARGET}.B99990001.pdb ptraj_full | tee clustering.log

echo "Deleting ptraj_trajin and ptraj_full"
rm ptraj_trajin ptraj_full

echo "Done."

echo "Creating averaged, optimized representatives of each cluster using MODELLER"
./create_clustered_representative.py -f ${TARGET}.B9999 -d ../modeling c4.txt
