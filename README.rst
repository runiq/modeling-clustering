Modeling with MODELLER and Clustering with ptraj
================================================

Requirements
============

- Python 2.7 (because of the ``argparse`` module, will try to change that in the
  future)
- BioPython
- Matplotlib


This is the setup I use to create protein models using MODELLER and cluster
them using ptraj. I've played around a lot with this, so please don't panic if
something doesn't work. If anything goes awry, shoot me a mail at <runiq at
archlinux dot us>.

Most of the script logic is in the ``runall.sh`` files which you'll have to run
after setting the appropriate parameters in them. If someone wants me to, I can
instead write some parsing logic for config files (I haven't felt the need to
since I'm intimately familiar with these scripts, but your mileage may vary, of
course).

Licensed under 2-clause BSD; see ``LICENSE.txt``.

1. General Procedure
====================

1.  Create decoys using MODELLER (``modeling`` directory)
2.  Either:
    a)  Set a critical RMSD distance threshold, or
    b)  Set the desired cluster number
         (``find-correct-clustering-number`` directory)
3.  Cluster using ptraj
4.  Either
    a)  Use the decoy which is closest to the consensus structure of the decoy
        (``<cluster_name>.rep.c<cluster_number>``), or
    b)  Minimize the actual consensus structure using MODELLER
        (``clustering/create_cluster_representative.py``)
5.  Choose the best cluster

2. Procedure in Detail
======================

2.1. Modeling (``modeling`` Directory)
--------------------------------------

Ingredients

- Alignment (``alignment.pir``)
- Modeling script (``modeller-parallel.py``)

Start with ``./runall.sh``. This will call ``modeller-parallel.py`` with the
parameters you set beforehand. If you want to adjust these or want to see which
parameters exist at all, call ``./modeller-parallel.py --help``.

2.2. Finding the Correct Cluster Number (``find-correct-clustering-number`` Directory)
--------------------------------------------------------------------------------------

There are two criteria you can use for clustering:

a)  Tell ptraj to put all clusters *up to a certain distance threshold* into a
    cluster. This is what MODELLER does with its ``cluster_xyz()`` function. It's
    probably sufficient in most basic cases, and 1–1.5Å are a good threshold.

    If you use this method, better err on the side of too small instead of too
    large when deciding on a distance threshold. Otherwise you risk including
    bad, non-native structures in your cluster (false positives).

b)  Set the cluster number yourself. This requires you to find a "good" cluster
    number. [Shao.etal2007]_ argue that the "best" cluster number is the one
    with the highest *information content*. The scripts supplied in this package
    allow you to choose your target cluster number based on the four metrics
    suggested in that paper; the critical distance, DBI, pSF, and SSR/SST ratio.

    A short blurb on how to interpret the metrics:
     -  Number of decoys in cluster should be high (according to [Shortle.etal1998]_)
     -  DBI should be low
     -  pSF should be high
     - SSR/SST should have an "elbow"

Ingredients for 2b):
-  ptraj for clustering
-  script to plot metrics (``plot_clustering_metrics.py``)
-  ``clustering_run.py`` and ``newick.py``
-  matplotlib library (should be installed on your computer)

Just run ``./runall.sh`` in the ``find-correct-cluster-number`` directory. If everything works, you should now have a file called ``clustering_metrics.png`` in which the metrics are plotted vs. the cluster number.

2.3. Clustering (``clustering`` Directory)
------------------------------------------

When you've found a good cluster number or a good RMSD threshold, you can
finally start your actual clustering run! Yay!

Ingredients:
-  Script that removes micromanaging (``cluster.sh``)
-  Script that supplies ptraj with the actual clustering command
   (``ptraj_cluster``)
-  script to create a representative structure from decoy PDBs and a ptraj
   clustering log file

All this can be done automatically by running ``./runall.sh`` in the
``clustering`` directory. After reading the ptraj paper [Shao.etal2007]_, I've
decided on the AverageLinkage algorithm (see ``ptraj_cluster`` script).

2.4. (Optional) Creating a Minimized Consensus Structure
--------------------------------------------------------

By default, ptraj can output the representative structure in PDB format if you
tell it to. This is the decoy in a cluster that is closest to the cluster's
consensus structure. If you actually want to work with the consensus structure,
however, note that it is not biologically relevant, so you'll have to minimize
before working with it. The ``create_clustering_representatives.py`` script
does that for you. 

2.5. Choosing the Best Cluster
------------------------------

Your methods are probably as good as mine, but here are the guidelines I
generally follow:

 -  Good clusters are probably better according to [Shortle.etal1998]_
 -  Evaluate all representative structures with the SwissModel-Server
    (http://swissmodel.expasy.org/workspace/index.php?func=tools_structureassessment1)
 -  Compare DOPE scores of models (script ``assess_dope.py``)
 -  Create Ramachandran plots and compare outliers using RAMPAGE
    (http://mordred.bioc.cam.ac.uk/~rapper/rampage.php)


3. Ptraj
========

3.1. General Stuff
------------------

If you want to read about ptraj in general and its ``cluster`` command in particular, I heartily recommend the AMBERTools manual. You basically do the following:

1.  Read in your PDB files with the ``trajin`` command (p. 109 in AMBERTools manual)

    I've done this programmatically in the scripts because I'd have to write
    2000 lines of ``trajin "../modeling/<model_name>.B9999????.pdb"``, which is
    not an activity I'd describe as "intellectually stimulating". (See
    ``find-correct-cluster-number/runall.sh``, the line below "Create ptraj
    input file" on how this is done.)

2. Cluster using the ``cluster`` command (p. 117 in AMBERTools manual).  If you
   look at the ``find-correct-cluster-number/runall.sh`` script, you'll notice
   I first cluster until all decoys are merged into a single cluster and then
   perform a number of additional clustering runs (up to a cluster number of
   20). This is done because the results of the first ``cluster`` command are
   cached in a file called ``ClusterMerging.txt``.  This file can be used with
   the ``ReadMerge`` pseudo algorithm when doing subsequent clustering runs,
   which we do, because we'd like to obtain the metrics in the clustering log
   file, thank you very much. The cache file can also be used to reconstruct
   the merge tree for the AverageLinkage algorithm, but the script for that is
   not yet fit for public consumption.

3. Call ptraj: ``ptraj <one_of_your_modeller_generated_pdb_files.pdb> ptraj_script``

   Unfortunately, the ``cluster`` command is not (yet) parallelized, so the first clustering run might take a while.

3.2. Ptraj Masks
----------------

When using ptraj, you can choose which atoms should be taken into consideration
when calculating the RMSD between your decoys. This is done using *masks*. By
default, I choose all main-chain atoms of residues 7–216 for superpositioning
and RMSD calculations. The corresponding mask looks like this:

    :7-216@N,CA,C,O

If you want to change the group of atoms to perform the clustering on (and you most certainly will), look no further than p. 267 of the AMBERTools manual.

References
==========

..  [Shao.etal2007] Shao, J.; Tanner, S. W.; Thompson, N. & Cheatham, T. E.
    **Clustering Molecular Dynamics Trajectories: 1. Characterizing the Performance
    of Different Clustering Algorithms** *Journal of Chemical Theory and
    Computation*, 2007, 3, 2312-2334.

..  [Shortle.etal1998] Shortle, D.; Simons, K. T. & Baker, D. **Clustering of
    low-energy conformations near the native structures of small proteins.** *Proc
    Natl Acad Sci USA*, Department of Biochemistry, University of Washington School
    of Medicine, Seattle, WA 98195, USA., 1998, 95, 11158-11162
