#!/bin/sh

TARGET=target
TEMPLATE=template
NUM_MODELS=2000

echo "Creating $NUM_MODELS models of $TARGET
- using alignment.pir as alignment
- using $TEMPLATE as template
- using all available CPUs
- using '.' as structure file directory"

./modeller-parallel.py -a alignment.pir -k $TEMPLATE -s $TARGET -d '.' -S 1 -E $NUM_MODELS
