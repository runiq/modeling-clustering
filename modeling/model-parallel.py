#!/usr/bin/env python
"""
Homology modeling by the automodel class, using multiple processors.

Requires MODELLER and an alignment file.
"""

from multiprocessing import cpu_count

from modeller import *
from modeller.automodel import *    # Load the automodel class
from modeller.parallel import *


def parse_args():
    import argparse as ap
    p = ap.ArgumentParser(description=__doc__,
            formatter_class=ap.RawDescriptionHelpFormatter)
    p.add_argument('-a', '--alignment-file', metavar='FILE',
            default='alignment.pir',
            help="Alignment file (default: alignment.pir)")
    p.add_argument('-k', '--knowns', metavar='SEQID', default='template',
            nargs='+',
            help="Sequence ID of the template sequence (default: template)")
    p.add_argument('-s', '--sequence', metavar='SEQID', default='target',
            help="Sequence ID of the target sequence (default: target)")
    p.add_argument('-d', '--atom-files-dir', nargs='+',
            default=['.'],
            help=("Directories for template PDB files (default: '.')")
    p.add_argument('-S', '--starting-model', default=1, type=int,
            help="Index of starting model (default: 1)")
    p.add_argument('-E', '--ending-model', default=2000, type=int,
            help="Index of starting model (default: 2000)")
    p.add_argument('-c', '--num-cpus', default=None,
            help="Number of CPUs to use (default: all)")

    return p.parse_args()


def main():
    args = parse_args()
    num_cpus = args.num_cpus or cpu_count()

    # Create worker processes
    j = job()
    for _ in xrange(num_cpus):
        j.append(local_slave())

    # I don't want memory output, thank you very much
    log.level(output=1, notes=0, warnings=1, errors=1, memory=0)

    env = environ()
    # Specified on the command-line
    env.io.atom_files_directory = args.atom_files_dir

    a = automodel(env,
                  alnfile  = args.alignment_file, # alignment filename
                  knowns   = args.knowns,         # codes of the templates
                  sequence = args.sequence)       # code of the target
    a.starting_model = args.starting_model        # index of the first model
    a.ending_model   = args.ending_model          # index of the last model
                                                  # (determines how many models to calculate)
    a.use_parallel_job(j)                         # Use the job for model building
    a.library_schedule = autosched.slow
    a.md_level = refine.very_slow                 # Use better model refinement step
    a.make()                                      # do the actual homology modeling


if __name__ == '__main__':
    main()
